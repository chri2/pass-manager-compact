# pass-manager-compact

This a compact GUI for pass (https://www.passwordstore.org/). It’s designed to be used for the Librem5 (or other Linux Smartphones with GTK). But it can also be used on normal screens.

It supports the extension pass-otp (https://github.com/tadfisher/pass-otp
and the extension pass-tomb (https://github.com/roddhjav/pass-tomb)


## Before using pass-manager-compact 
To use it you first have to setup pass
"pass init {your pgp-key}"


## Special notes for using the extension pass-tomb 
Only Librem5 I could run pass-manager-compact directly from the application list.
On Debian Desktop I had to run pass-manager-compact from shell as tomb requires to enter a password.


## Special notes for Librem 5 with PureOS
The copy to clipboard mode in pass 1.7.3 doesn't work out of the box.
You have to install wl-clipboard Version 2.0.0 or higher (http://repo.pureos.net/pureos/pool/main/w/wl-clipboard/)
and wl-clipboard-x11 (https://github.com/brunelli/wl-clipboard-x11/).


## License
### GNU GPL v3.0

[![GNU GPL v3.0](http://www.gnu.org/graphics/gplv3-127x51.png)](http://www.gnu.org/licenses/gpl.html)
