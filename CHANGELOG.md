## V1.3 (2023-05-19)
- Added handling if tomb is already open
- Added 'X-Purism-FormFactor=Workstation;Mobile;' to show that pass-mgr-compact is mobile friendly 

## V1.2 (2022-12-18)
- Fixed progress bar if a password is in clipboard and another one is retrieved
- Added option for "pass open --force"
- Added some popups if errors occurs

## V1.1 (2021-02-07)
- Optimized Scalling for Librem5
- Swtiched from os.system/os.popen to subprocess

## V1.0 (2020-12-28)
- Rounded "Last modified" to only one digit.
- Successfully tested on a pyhsical Librem5

## V0.6 (2020-11-19)
- Switched to python3 incl. fixes for compatibility

## V0.5 (2020-07-19)
- Added Progress Bar to visualize when the password is available in clipboard
- Fixed action "New" when no existing element is selected

## V0.4 (2020-01-04)
- Fixed collapsing folders after delete/save

## V0.3 (2020-01-03)
- Fix to handle an empty password store
- Enabled double click for open/close folders
- Enabled double click to copy passwords to clipboard
- Moved Delete Button from "edit" view to main view
- Added support for "pass tomb"

## V0.2 (2019-12-23)
- Check for "pass" / "pass otp" / passwordstore
- Deactivate OTP feature if not available
- Added Delete button
- Restricted edit to Type/Password

## V0.1 (2019-12-20)
- Initial release
